/*
**
** fetches record id from bugs[dot]chromium[dot]org
** set the url & it will get the job done.
**
*/



const fs = require('fs');
const cheerio = require('cheerio');
const _ = require('lodash');
const request = require('request-promise');
const waggish = require('waggish');



async function main() {

  // we have fetching 1100 records please use the test url for testing.
  // checkout the arg num=1100 in url for more info.
  const url = "https://bugs.chromium.org/p/project-zero/issues/list?can=1&q=&colspec=ID%20Type%20Status%20Priority%20Milestone%20Owner%20Summary&num=1100&start=0"

  //for testing fetching only 100 records
  // const url = "https://bugs.chromium.org/p/project-zero/issues/list?can=1&q=&colspec=ID%20Type%20Status%20Priority%20Milestone%20Owner%20Summary&num=100&start=0"

  let options = {
      uri: url,
      transform: function (body) {
          return cheerio.load(body);
      }
  };

  //bugs[dot]chromium[dot]org
  let $ = await request.get(options);

  // let cursorarea = $('#cursorarea');
  let ids = [];
  let tbody = $('#cursorarea tbody');
  tbody.find('tr').each(function (i, el) {
    let $tds = $(this).find('td');
    let id = $tds.eq(1).children().text().trim()
    // console.log(`${id},`);
    ids.push(id);
  })



  // console.log(ids);

  await fs.appendFileSync(`./data/ids_stage_4.csv`, ids);



}//main ends


main();
