/*
**
** fetches record from zerodayinitiative[dot]com and nvd[dot]nist[dot]gov
** script set the year and number of record inside the script & it will get the job done.
**
*/

const fs = require('fs');
const cheerio = require('cheerio');
const _ = require('lodash');
const request = require('request-promise');
const waggish = require('waggish');



async function main() {

  const limit = 4 //<======= set this limit => ZDI-year-{limit}
  const year = 15 //<======= set the year - 2 digits

  for (let i = 5; i < 9 ; i++) {
    console.log(`record ${i} fetching ...`);

    let title, ZDID, vendorDate, publicDate;
    let CVEID = 'CVE:---------';

    let id = `ZDI-${year}-${(`00` + i).slice(-3)}`;
    let url = `http://www.zerodayinitiative.com/advisories/${id}`;

    // let url = `http://www.zerodayinitiative.com/advisories/ZDI-16-682/`;

    let options = {
        uri: url,
        transform: function (body) {
            return cheerio.load(body);
        }
    };

    //zerodayinitiative
    let $ = await request.get(options);

    let mainContent = $('#main-content');

    title = mainContent.children().eq(0).text().trim().replace(",", "-");
    ZDID = mainContent.children().eq(1).text().trim();

    mainContent.find('h4').each(function (i, el) {

      if($(this).text() == "CVE ID"){
        CVEID = $(this).next().children().text() ;
        // console.log(`CVEID ${CVEID}`);

      }


      if($(this).text() == "Disclosure Timeline"){
        let dateList = $(this).next().text();
        vendorDate = dateList.split(" -")[0].trim();
        publicDate = dateList.split(" -")[1].split('\n')[1].trim();

        // console.log(`vendorDate ${vendorDate}`);
        // console.log(`publicDate ${publicDate}`);

      }

    });

    let row = await _.join([ title, ZDID, CVEID, vendorDate, publicDate ], ',');
    // console.log(row);


    //National Vulnerability Database !
    if (CVEID != 'CVE:---------') {

      url = `https://nvd.nist.gov/vuln/detail/${CVEID}`

      options = {
          uri: url,
          transform: function (body) {
              return cheerio.load(body);
          }
      };

      $ = await request.get(options);

      let scoreAndRanking = $('[data-testid="vuln-cvssv2-base-score"]').children().text().trim();
      scoreAndRanking = scoreAndRanking.replace(/(\r\n|\n|\r)/gm,"").replace(/ +(?= )/g,'')
      let score = scoreAndRanking.split(' ')[0];
      let ranking = scoreAndRanking.split(' ').pop();
      let vulnerabilityType = $('[data-testid="vuln-technical-details--1"]').children().text().trim().split('(')[0].replace(",", "-");
      let CWE = $('[data-testid="vuln-technical-details--1"]').children().text().trim().split('(').pop().trim();
      CWE = CWE && CWE.replace(')', '').split('-').pop();
      row += _.join([ score, ranking, vulnerabilityType, CWE], ',');

    }

    console.log(`record ${i} writing ...`);

    // console.log(row);
    await fs.appendFileSync(`./data/${year}_stage_3.csv`, `${row} \n`);


    //this will print some rendom message so ppl don't die by boredom
    if (i%7 === 0) {
      console.log(waggish.random());
    }

  }

}//main ends


main();
