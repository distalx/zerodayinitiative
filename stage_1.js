const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const _ = require('lodash');
const rp = require('request-promise');

/* zerodayinitiative url format
** `http://www.zerodayinitiative.com/advisories/${id}`;
**  where id is dynemic `ZDI-16-001`;
**  set the limit and ...
*/

const limit = 4 //<======= set this limit
const year = 17 //<======= set the year - 2 digits

for (let i = 3; i < limit ; i++) {

  let id = `ZDI-${year}-${(`00` + i).slice(-3)}`;
  // let url = `http://www.zerodayinitiative.com/advisories/${id}`;
  let url = `http://www.zerodayinitiative.com/advisories/ZDI-16-683/`;


  let options = {
      uri: url,
      transform: function (body) {
          return cheerio.load(body);
      }
  };

  rp(options)
  .then(function ($) {
    let mainContent = $('#main-content');

    let title = mainContent.children().eq(0).text();
    let ZDI = mainContent.children().eq(1).text();

    mainContent.find('h4').each(function (i, el) {

      if($(this).text() == "CVE ID"){
        let CVEID = $(this).next().children().text();
        console.log(`CVEID ${CVEID}`);

      }
      if($(this).text() == "Disclosure Timeline"){
        let dateList = $(this).next().text();
        let vendorDate = dateList.split(" -")[0].trim();
        let publicDate = dateList.split(" -")[1].split('\n')[1].trim();

        console.log(`vendorDate ${vendorDate}`);
        console.log(`publicDate ${publicDate}`);

      }

    });

    console.log(`TITLE :  ${title}`);
    console.log(`ZDI ${ZDI}`);



    // let row = _.join([ title, ZDI, CVE, '\n' ], ',');
    // console.log(row);

  })
  .catch(function (err) {
    console.log(err);
  });


} //FOR LOOP ENDS
