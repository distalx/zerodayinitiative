const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const _ = require('lodash');
const rp = require('request-promise');
const parse = require('csv-parse');

/*
** this script is scraping a data from
** specify input file name - (from stage 1 an it will does it thing)
**
*/


const file_name = "stage_2_test";


fs.createReadStream(`./data/${file_name}.csv`)
  .pipe(parse({delimiter: ','}))
  .on('data', function(csvrow) {
      // console.log(csvrow);

      if (csvrow[4] != 'CVE:---------') {

					let url = `https://nvd.nist.gov/vuln/detail/${csvrow[4]}`

					console.log(url);

			    var options = {
			        uri: url,
			        transform: function (body) {
			            return cheerio.load(body);
			        }
			    };


					//featch data
		      rp(options)
		      .then(function ($) {


						//cherrypick
		        let CVE = $('[data-testid="page-header-vuln-id"]').text().trim();
		        let scoreAndRanking = $('[data-testid="vuln-cvssv2-base-score"]').children().text().trim();
		        scoreAndRanking = scoreAndRanking.replace(/(\r\n|\n|\r)/gm,"").replace(/ +(?= )/g,'')
		        let score = scoreAndRanking.split(' ')[0];
		        let ranking = scoreAndRanking.split(' ').pop();
		        let vulnerabilityType = $('[data-testid="vuln-technical-details--1"]').children().text().trim();

		        let row = _.join([ CVE, score, ranking, vulnerabilityType, '\n' ], ',');
						console.log(row);


		        // fs.appendFileSync(`./data/${file_name}_output.csv`, row);
						//write
		        fs.appendFile(`./data/${file_name}_output.csv`, row, function (err) {
		          if (err) throw err;
		          console.log('record saved!');
		        });

		      })
		      .catch(function (err) {
		          console.log(err);
		          Error(err);

		      });



      }

  })
  .on('end',function() {
    //end of csv
    console.log('eof');
  });
