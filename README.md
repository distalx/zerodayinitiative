scrapper for zerodayinitiative [dot] com, using cheerio


```
npm install

node stage_0.js
node stage_1.js
node stage_2.js
node stage_3.js
node stage_4.js
node stage_5.js

```

stage_0.js

it scraps the list of published advisories http://www.zerodayinitiative.com/advisories/published/2016/

stage_1.js (under construction)

it wasn't planned earlier,
but the record on published advisories page is recurring search for `CVE: CVE-2016-5805` on
http://www.zerodayinitiative.com/advisories/published/2016/

stage_2.js

it reads the output of stage 1 and fetches the relevant information National Vulnerability Database !


stage_3.js

fetches record from zerodayinitiative[dot]com and nvd[dot]nist[dot]gov
script set the year and number of record inside the script & it will get the job done.



stage_4.js
gets a list of issue id from
"https://bugs.chromium.org/p/project-zero/issues/list?can=1&q=&colspec=ID%20Type%20Status%20Priority%20Milestone%20Owner%20Summary&num=1100&start=0"



stage_5.js
