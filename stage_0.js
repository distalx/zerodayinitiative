const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const _ = require('lodash');

/*
**
** spcify the year and it will get the job done
** zerodayinitiative
*/

const year = 2015;  ///<<<============ here change the year

const url = `http://www.zerodayinitiative.com/advisories/published/${year}/`;

request(url, function(error, response, html){
  if(error){
    new Error(error);
  }

  const $ = cheerio.load(html);


  let published , ZDI, CVE, title, ZDIURL;

  //list table
  const tbody = $('table tbody');
  tbody.find('tr').each(function (i, el) {

      let bgcolor = $(this).attr('bgcolor');
      let $tds = $(this).find('td');

      if (bgcolor == '#DDDDDD') {

        ZDI = $tds.eq(0).children().text().trim();
        CVE = $tds.eq(1).text().split(':').pop().trim() ? $tds.eq(1).text().split(':').pop().trim() : $tds.eq(1).text().trim() + '---------' ;
        published = $tds.eq(2).text().split(':').pop().trim();

      }
      if (bgcolor == '#EEEEEE') {
        ZDIURL = 'http://www.zerodayinitiative.com' + $tds.eq(0).children().attr('href');
        title = $tds.eq(0).children().text().trim();
      }

      if (bgcolor == '#FFFFFF') {
        //do nothing
        let row = _.join([ published, ZDI, ZDIURL, title, CVE , '\n' ], ',');
        fs.appendFileSync(`./data/${year}_stage_1.csv`, row);
      }

  });

});
